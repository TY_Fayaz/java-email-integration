package com.technoelevate.email.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailAttachmentDTO {
	private String toEmail;
	private String subject;
	private String body;
	private String attachment;
	private String from;
}
