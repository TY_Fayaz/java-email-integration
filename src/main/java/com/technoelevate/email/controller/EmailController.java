package com.technoelevate.email.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technoelevate.email.dto.EmailDTO;
import com.technoelevate.email.dto.SmsDTO;
import com.technoelevate.email.entity.EmailAttachment;
import com.technoelevate.email.service.EmailSenderService;

@RestController
public class EmailController {
	@Autowired
	private EmailSenderService service;

	@PostMapping("/send")
	public ResponseEntity<String> sendEmail(@RequestBody EmailDTO emailDto) {
		service.sendEmail(emailDto.getToEmail(), emailDto.getSubject(), emailDto.getBody(),emailDto.getFrom());
		return ResponseEntity.ok("success");
	}

	@PostMapping("/sendAttachment")
	public ResponseEntity<String> sendAttachment(@RequestBody EmailAttachment emailAttDto) {

		service.sendAttachment(emailAttDto.getToEmail(), emailAttDto.getSubject(), emailAttDto.getBody(),
				emailAttDto.getAttachment(), emailAttDto.getFrom());
		return ResponseEntity.ok("success");

	}
	
	/*
	 * @PostMapping("/sms") public ResponseEntity<String> sendSms(@RequestBody
	 * SmsDTO smsDto){ try {
	 * service.sendSms(smsDto.getMessage(),smsDto.getNumber()); } catch(Exception
	 * e){ e.printStackTrace(); } return ResponseEntity.ok("success"); }
	 */
}
