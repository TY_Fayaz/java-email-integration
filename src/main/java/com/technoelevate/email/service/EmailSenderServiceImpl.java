package com.technoelevate.email.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {

	@Autowired
	private JavaMailSender mailSender;

	@Override
	public void sendEmail(String toEmail, String subject, String body,String from) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setFrom(from);
		mailMessage.setTo(toEmail);
		mailMessage.setText(body);
		mailMessage.setSubject(subject);
		mailSender.send(mailMessage);
		System.out.println("mail send successfully");
	}

	public void sendAttachment(String toEmail, String subject, String body, String attachment, String from) {
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(from);
			helper.setTo(toEmail);
			helper.setSubject(subject);
			helper.setText(body);
            //for sending the attachment
			FileSystemResource file = new FileSystemResource(attachment);
			helper.addAttachment(file.getFilename(), file);
			mailSender.send(message);
			System.out.println("email send successfully");
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}

	}

	/*
	 * @Override public void sendSms(String message, Long number) { try { String
	 * apiKey=
	 * "oFJ58AGjP3krcLsIyMUKnDS9viB2X641lamVeQW0btEqTR7NYONYqpbE32f0zmVDTn1MS7l5sPidaeyZ";
	 * String senderId="TXTIND"; message=URLEncoder.encode(message,"UTF-8"); String
	 * language="english"; String route="v3"; String
	 * myUrl="https://www.fast2sms.com/dev/bulkV2?authorization="+apiKey+
	 * "&sender_id="+senderId+"&message="+message+"&route="+route+"&numbers="+
	 * number; //sending get request using java URL url=new URL(myUrl);
	 * HttpsURLConnection con=(HttpsURLConnection)url.openConnection();
	 * con.setRequestMethod("POST");
	 * con.setRequestProperty("User-Agent","Google Chrome/107.0.5304.122");
	 * con.setRequestProperty("cache-control","no-cache"); int
	 * code=con.getResponseCode(); System.out.println(myUrl);
	 * System.out.println("response code"+code); StringBuffer response=new
	 * StringBuffer(); BufferedReader br=new BufferedReader(new
	 * InputStreamReader(con.getInputStream())); while(true) { String
	 * line=br.readLine(); if(line==null) { break; } response.append(line); }
	 * 
	 * System.out.println(response); } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * }
	 */
}
