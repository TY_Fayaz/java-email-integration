package com.technoelevate.email.service;

public interface EmailSenderService {
	public void sendEmail(String toEmail, String subject, String body,String from);

	public void sendAttachment(String toEmail, String subject, String body, String attachment, String from);

//	public void sendSms(String message, Long number);
}
